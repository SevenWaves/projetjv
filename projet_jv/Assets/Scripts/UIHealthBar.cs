using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class UIHealthBar : MonoBehaviour
{
    [SerializeField] private RectTransform fill;
    [SerializeField] private EnemyCharacter enemy;
    
    private Camera _mainCamera;

    public float Fill
    {
        get => fill.anchorMax.x;
        set
        {
            var anchorMax = fill.anchorMax;
            anchorMax.x = value;
            fill.anchorMax = anchorMax;
        }
    }
    
    void Start()
    {
        _mainCamera = Camera.main;
    }

    private void Update()
    {
        transform.LookAt(_mainCamera.transform);
        //print( 1 - (enemyCharacter.GetHealth() / enemyCharacter.GetHealthMax()));
    }

    public void SetFill()
    {
            gameObject.SetActive(true);
            Fill = (enemy.GetHealth() / enemy.GetHealthMax()) ;
    }
}
