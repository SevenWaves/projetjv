using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingItem : MonoBehaviour
{
    [SerializeField] private GameObject heal;
    [SerializeField] private PlayerCharacter player;
    [SerializeField] private float hp;
   // [SerializeField] private Animator animator;

   // private int _healTriggerHash;
    
    // Start is called before the first frame update
    void Start()
    {
       // _healTriggerHash = Animator.StringToHash("Heal");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 6){
        player.restoreHealth(hp);
        FindObjectOfType<AudioManager>().Play("heal");
        GameObject explosion = Instantiate(heal, transform.position, transform.rotation);
        explosion.SetActive(true);
        Destroy(gameObject);
        }
    }


}
