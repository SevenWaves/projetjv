using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerCharacter : MonoBehaviour, IDamageable
{
    [SerializeField] private float playerSpeed = 8f;
    [SerializeField] private float runSpeed = 12f;
    [SerializeField] private float rotationSpeed = 180f;
    [SerializeField] private float gravity = -20f;
    [SerializeField] private float jumpSpeed = 15;
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject bullet;
    [SerializeField] private Bullet bulletScript;
    [SerializeField] private Transform bulletSpawn;
    [SerializeField] private float cooldown = 100f;

    [SerializeField] HP healthBar;

    private float currentSpeed;

    [SerializeField] private float sensitivity = 15f;
    private float rotationX = 0f;
    private float rotationY = 0f;

    [SerializeField] private GameObject explosion;
    [SerializeField] private float _healthMax = 100f;
    private float health;
    [SerializeField] private GameObject mesh;

    private bool isRunning = false;
    private bool isJumping = false;

    private float bulletSpeed = 25f;
    private float damage = 10f;



    private float ySpeed;
    
    private float _latestShootTime;

    private Camera _mainCamera;
    private CharacterController _characterController;
    private float originalStepOffset;

    Vector3 moveVelocity;
    Vector3 moveHVelocity;
    Vector3 turnVelocity;

    protected void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        _mainCamera = Camera.main;
        _characterController = GetComponent<CharacterController>();
        originalStepOffset = _characterController.stepOffset;
        health = _healthMax;
        FindObjectOfType<AudioManager>().Play("music");
    }

    protected void Update()
    {
        Look();
        //DEPLACEMENTS--------------------------------------------------
        Move();

        //TIR----------------------------------------------------
        if (Input.GetMouseButtonDown(1) && (Time.time - _latestShootTime) > cooldown)
        {
            Shoot();
            _latestShootTime = Time.time;
        }



    }

    private void Shoot()
    {
        bulletScript.setDamage(damage);
        bulletScript.setSpeed(bulletSpeed);
        GameObject projectile = Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
        projectile.SetActive(true);
        FindObjectOfType<AudioManager>().Play("shoot");
    }

    private void Look()
    {
        rotationY+= Input.GetAxis("Mouse X")*sensitivity;
        rotationX+= Input.GetAxis("Mouse Y")*sensitivity*(-1);
        transform.localEulerAngles = new Vector3(0,rotationY,0);
    }

    private void Move()
    {
        var hInput = Input.GetAxis("Horizontal");
        var vInput = Input.GetAxis("Vertical");
        if(isRunning){
            if(vInput==0)
            {
                _animator.SetBool("isRunning", false);
                isRunning=false;
                FindObjectOfType<AudioManager>().StopPlaying("steps");
            }
        }
        else
        {
            if(vInput!=0)
            {
                _animator.SetBool("isRunning", true);
                isRunning=true;
                FindObjectOfType<AudioManager>().Play("steps");
            }
        }
        //if(vInput!=0){_animator.SetBool("isRunning", true);}
        //else{_animator.SetBool("isRunning", false);}
        if(Input.GetKey(KeyCode.LeftShift)){currentSpeed=runSpeed;}
        else{currentSpeed=playerSpeed;}
        moveVelocity = transform.forward * currentSpeed * vInput;
        moveHVelocity = transform.right * (-hInput) * playerSpeed;
        turnVelocity = transform.up * rotationSpeed * hInput;
        ySpeed += gravity * Time.deltaTime;
        if (_characterController.isGrounded)
        {
            ySpeed = -0.5f;

            if (isJumping)
            {
                isJumping = false;
                _animator.SetBool("isJumping", false);
                if(isRunning){FindObjectOfType<AudioManager>().Play("steps");}
            }

            if (Input.GetButtonDown("Jump"))
            {
                isJumping=true;
                ySpeed = jumpSpeed;
                FindObjectOfType<AudioManager>().StopPlaying("steps");
                FindObjectOfType<AudioManager>().Play("jump");
                _animator.SetBool("isJumping", true);
            }
        }
        
        moveVelocity.y = ySpeed;
        moveHVelocity.y = ySpeed;
        _characterController.Move(moveVelocity*Time.deltaTime);
        _characterController.Move(moveHVelocity*Time.deltaTime);
        //transform.Rotate(turnVelocity * Time.deltaTime);
    }

    public void ApplyDamaged(float value)
    {
        //changeColor.ChangeColor();
        health -= value;
        if (health <= 0f)
        {   
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(mesh);
            Destroy(this);
            //spawner.DecrNbEnemy();
            //score.nbKill += 1;
            //score.ChangeScore();
        }
        healthBar.SetFill();
    }

    public float GetHealth()
    {
        return health;
    }
    
    public float GetHealthMax()
    {
        return _healthMax;
    }

    public void restoreHealth(float heal)
    {
        health = Mathf.Min(_healthMax,health+heal);
        healthBar.SetFill();
    }

    public void addDamage(float toAdd)
    {
        damage += toAdd;
    }

    public void addBulletSpeed(float toAdd)
    {
        bulletSpeed += toAdd;
    }

    public void setCadence(float cadence)
    {
        cooldown = cadence;
    }

    public void addRunSpeed(float rs)
    {
        runSpeed += rs;
    }

    public void addPVMax(float pvm)
    {
        _healthMax+=pvm;
        health+=pvm;
        healthBar.SetFill();
    }




}







        /*
        if (Input.GetMouseButton(0))
        {

            var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

            var plane = new Plane(Vector3.up, Vector3.zero);
            if (plane.Raycast(ray, out var x))
            {
                var targetPosition = ray.GetPoint(x);
                var position = transform.position;

                var directionToTarget = targetPosition - position;

                var dot = Vector3.Dot(transform.forward, directionToTarget.normalized);
                var speedPenalty = (dot + 1f) / 2f;

                var newPosition = Vector3.MoveTowards(position, targetPosition, speedPenalty * speed * Time.deltaTime);
                _characterController.Move(newPosition - position);

                transform.rotation = Quaternion.RotateTowards(
                    transform.rotation,
                    Quaternion.LookRotation(directionToTarget),
                    angularSpeed * Time.deltaTime);
            }
        }
        */