using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ChangementCouleur : MonoBehaviour
{
    public List<Material> _materials = new List<Material>();
    public List<Color> _initMaterialsColor = new List<Color>();
    [SerializeField] private AnimationCurve curve;

    private bool done = false;

    // Start is called before the first frame update
    void Start()
    {
        Renderer[] renderer = GetComponentsInChildren<Renderer>();
        foreach (var render in renderer)
        {
            foreach (var mat in render.materials)
            {
                _materials.Add(mat);
                _initMaterialsColor.Add(mat.color);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            /*foreach (var material in _materials)
            {
                material.color = Color.white;             
            }*/

            //StartCoroutine("ColorCoroutine");
        }
    }
    public void redHead()
    {
        Invoke("redhead",0.1f);
    }

    public void redhead()
    {
        var material = _materials[3];
        material.color = Color.red;
        var material2 = _materials[8];
        material2.color = Color.red;
    }
    public void allBlack()
    {
        Invoke("allblack",0.2f);
    }

    public void allblack()
    {
        var material = _materials[0];
        material.color = Color.black;
        var material2 = _materials[1];
        material2.color = Color.black;
        var material3 = _materials[2];
        material3.color = Color.black;
        var material4 = _materials[3];
        material4.color = Color.black;
        var material5 = _materials[4];
        material5.color = Color.black;
        var material6 = _materials[5];
        material6.color = Color.black;
        var material7 = _materials[6];
        material7.color = Color.black;
        var material8 = _materials[7];
        material8.color = Color.black;
        var material1 = _materials[8];
        material1.color = Color.black;
    }

        public void allRed()
    {
        Invoke("allred",0.3f);
    }

    public void allred()
    {
        var material = _materials[0];
        material.color = Color.red;
        var material2 = _materials[1];
        material2.color = Color.red;
        var material3 = _materials[2];
        material3.color = Color.red;
        var material4 = _materials[3];
        material4.color = Color.red;
        var material5 = _materials[4];
        material5.color = Color.red;
        var material6 = _materials[5];
        material6.color = Color.red;
        var material7 = _materials[6];
        material7.color = Color.red;
        var material8 = _materials[7];
        material8.color = Color.red;
        var material1 = _materials[8];
        material1.color = Color.red;
    }

    public void ChangeColor()
    {
        StartCoroutine("ColorCoroutine");
    }

    IEnumerator ColorCoroutine()
    {
        const float duration = 0.5f;
        var timeLeft = duration;
        
        if(!done)
        {
            Renderer[] renderer = GetComponentsInChildren<Renderer>();
            foreach (var render in renderer)
            {
                foreach (var mat in render.materials)
                {
                    _materials.Add(mat);
                    _initMaterialsColor.Add(mat.color);
                }
            }
            done=true;
        }

        while (timeLeft > 0f)
        {
            var lerpValue = timeLeft > duration / 2f
                ? 2f * (1f - timeLeft / duration)
                : 2f * timeLeft / duration;
            
            
            for (var i = 0; i < _materials.Count; i += 1)
            {
                var material = _materials[i];

                material.color = Color.Lerp(_initMaterialsColor[i], Color.white, curve.Evaluate(duration - timeLeft));
            }
            
            timeLeft -= Time.deltaTime;
            yield return null;
        }
        

    }
}
