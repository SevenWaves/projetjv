using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;
using TMPro;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private PlayerCharacter perso;
    
    private int nbEnemyMax = 5;

    [SerializeField] private int _nbEnemy = 0;
    [SerializeField] private int toSpawn=2;
    private int round=1;
    private int score=0;
   
    [SerializeField] private TextMeshProUGUI roundText;
    [SerializeField] private TextMeshProUGUI scoreText;

    public void SetNbEnemyMax(int i)
    {
        nbEnemyMax = i;
    }
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Spawn", Random.Range(0.5f, 3f));
    }
    

    void Spawn()
    {
        if (toSpawn>0)
        {
            var newTarget = Instantiate(enemy);
            newTarget.transform.position = new Vector3(Random.Range(-20, 20), 0f, Random.Range(-20, 20));
            //StartCoroutine(SpawnCoroutine(newTarget));
            newTarget.SetActive(true);
            _nbEnemy += 1;
            toSpawn-=1;
        }
        
        Invoke("Spawn", Random.Range(0.3f, 1f));
        
    }

    private IEnumerator SpawnCoroutine(GameObject target)
    {
        while (target.transform.position.y > 0)
        {
            target.transform.position -= 50f * Time.deltaTime * Vector3.up;
            //print(transformPosition);
            yield return null;
        }
    }

    public void DecrNbEnemy()
    {
        _nbEnemy -= 1;
        if (_nbEnemy==0 && toSpawn==0)
        {
            newRound();
        }
        score+=100;
        scoreText.text = score.ToString();
    }

    void newRound()
    {
        round+=1;
        roundText.text = round.ToString();
        toSpawn=2*round;
        FindObjectOfType<AudioManager>().Play("laugh");
        perso.addBulletSpeed(5f);
        perso.addDamage(2f);
        perso.setCadence(1f/round);
        perso.addRunSpeed(1f);
        perso.addPVMax(5f);
    }

    public int getRound()
    {
        return(round);
    }
}
