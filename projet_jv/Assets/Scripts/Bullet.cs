using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float bulletSpeed = 10f;
    [SerializeField] private GameObject explosionFX;
    [SerializeField] private float damaged = 10f;

    private Collider _collider;

    private void Start()
    {
        Destroy(gameObject,3f);
    }

    void Update()
    {
        transform.position += transform.forward * (bulletSpeed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<IDamageable>(out var enemyCharacter))
        {
            enemyCharacter.ApplyDamaged(damaged);
        }
        
        GameObject explosion = Instantiate(explosionFX, transform.position, transform.rotation);
        explosion.SetActive(true);
        Destroy(gameObject);
    }

    public void setSpeed(float s)
    {
        bulletSpeed = s;
    }

    public void setDamage(float d)
    {
        damaged = d;
    }
}
