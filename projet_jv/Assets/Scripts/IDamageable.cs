using UnityEngine;

public interface IDamageable
{ 
    void ApplyDamaged(float value);

    float GetHealth();
    
    float GetHealthMax();
}
