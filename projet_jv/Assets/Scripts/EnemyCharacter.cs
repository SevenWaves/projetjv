using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyCharacter : MonoBehaviour, IDamageable
{
    private NavMeshAgent _navMeshAgent;

    [SerializeField] private GameObject explosion;
    [SerializeField] private float health = 30f;
    private float _healthMax;

    [SerializeField] private GameObject player;
    [SerializeField] private GameObject bullet;
    [SerializeField] private float angularSpeed = 360f;
    [SerializeField] private Transform bulletSpawn;
    [SerializeField] private Animator _animator;
    [SerializeField] private GameObject heal;
    [SerializeField] private UIHealthBar healthBar;
    [SerializeField] private EnemySpawner spawner;
    [SerializeField] private ChangementCouleur changeColor;

    [SerializeField] private Bullet bulletScript;
    //private int enemyType = 1;

    private float damage = 10f;
    private float bulletSpeed= 20f;

    private int round;

    protected void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _healthMax = health;
        
        round =  spawner.getRound();

        if (round>2)
        {
            if (Random.Range(Mathf.Min(6,round),10)>8f)
            {
                changeColor.redHead();
                health = 50f;
                _healthMax = 50f;
                _navMeshAgent.speed = 14f;
                angularSpeed = 480f;
                //enemyType=2;
                damage=13f;
                bulletSpeed=30f;
            }
        }

        if (round>4)
        {
            if (Random.Range(Mathf.Min(15,round),20)>18f)
            {
                changeColor.allBlack();
                health = 100f;
                _healthMax = 100f;
                _navMeshAgent.speed = 4f;
                angularSpeed = 180;
                //enemyType=3;
                damage = 60f;
                bulletSpeed=12f;
            }
        }

        if (round>6)
        {
            if (Random.Range(Mathf.Min(15,round),20)>18.9f) //TODO CHANGE
            {
                changeColor.allRed();
                health = 10*round;
                _healthMax = 10*round;
                _navMeshAgent.speed = round;
                angularSpeed = 360f;
                //enemyType=3;
                damage = 5*round;
                bulletSpeed=3*round;
            }
        }

    }

    protected void OnEnable()
    {
        StartCoroutine(Coroutine());
        IEnumerator Coroutine()
        {
            yield return null;
            _navMeshAgent.enabled = true;

            while (enabled)
            {
                _animator.SetBool("isRunning", true);
                _navMeshAgent.SetDestination(new Vector3(
                    Random.Range(Mathf.Max(-20f,transform.position.x-5f), Mathf.Min(20f,transform.position.x+5f)),
                    0f,
                    Random.Range(Mathf.Max(-20f,transform.position.z-5f), Mathf.Min(20f,transform.position.z+5f))));

                do yield return null;
                while (_navMeshAgent.hasPath);

                // Destination reached, wait before moving again.
                _animator.SetBool("isRunning", false);
                var newpt = transform.position;
                newpt.x = player.transform.position.x;
                //newpt.y = player.transform.position.y;
                newpt.z = player.transform.position.z;
                var directionToTarget = newpt - transform.position;
                while (Math.Abs(Vector3.Dot(transform.forward, directionToTarget.normalized) - 1) > 0.00001)
                {
                    transform.rotation = Quaternion.RotateTowards(
                        transform.rotation,
                        Quaternion.LookRotation(directionToTarget),
                        angularSpeed * Time.deltaTime);

                    yield return null;
                }

                Shoot();

                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    private void Shoot()
    {  
        /*
                RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            if (hit.collider.gameObject.tag == "wall" )
            {
                //print("mur");
            }
            
            else
            {
                Quaternion rota = transform.rotation;
                GameObject projectile = Instantiate(bullet, bulletSpawn.transform.position, rota);
                projectile.SetActive(true);
                FindObjectOfType<AudioManager>().Play("enemyshoot");
            }
        }
        
        else
        {       
            */   
            bulletScript.setDamage(damage);
            bulletScript.setSpeed(bulletSpeed);
            Quaternion rota = transform.rotation;
            GameObject projectile = Instantiate(bullet, bulletSpawn.transform.position, rota);
            projectile.SetActive(true);
            FindObjectOfType<AudioManager>().Play("enemyshoot");
       // }
    }


    public void ApplyDamaged(float value)
    {
        changeColor.ChangeColor();
        health -= value;
        if (health <= 0f)
        {   
            FindObjectOfType<AudioManager>().Play("deadzombi");
            if(Random.Range(0,10f)>8.9f)
            {
                GameObject newheal = Instantiate(heal, transform.position+new Vector3(0,1f,0), transform.rotation);
                newheal.SetActive(true);
            }
            spawner.DecrNbEnemy();
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
        healthBar.SetFill();
    }

    public float GetHealth()
    {
        return health;
    }
    
    public float GetHealthMax()
    {
        return _healthMax;
    }

}
