# projetJV


## Principe
Le joueur apparaît dans un jardin hanté et doit survivre à des vagues de zombies. Il aura à disposition des obstacles pour s’abriter et des soins occasionnellement lachés par les ennemis morts.
Au fil du temps les ennemis seront de plus en plus nombreux et seront plus coriace (plus de vie, plus de dégât, plus rapide), mais le joueur gagne également en puissance (dégât des tirs, cadence de tir, vitesse des projectiles, point de vie, vitesse de sprint)
Le joueur doit survivre le plus de manches possible et réaliser le plus haut score.

## Caméra  
Vue de la 3e personne derrière le joueur.

## Contrôle 
ZQSD pour le déplacement, barre espace pour sauter, Maj pour courir, la souris pour viser, clic-droit pour tirer.

## UI
Une barre de vie en HUD pour le joueur, une barre de vie pour les ennemis s’ils sont blessés.

## Avancement de la partie
A chaque fin de manche le joueur obtient un bonus de vitesse de sprint, de hp max, de dégats, de cadence de tir, et de vitesse des balles.
De nouveaux zombies apparaissent à partir de certaines manches. Il existe:
-Le basique (dès la manche 1)  
-Le basique à tête rouge: plus rapide, plus d'HP, plus de dégats, balles tirées plus rapides.  
-Le zombie noir: plus lent, beaucoup plus de dégats, beaucoup plus d'HP, balles tirées plus lentes  
-Le zombie rouge: ses statistiques dépendent de la manche, c'est le zombie qui assure la fin de partie puisque chaque manche lui fait gagner des dégats, de la vitesse, de la vitesse des balles tirées, et des HP max.  
Le nombre de zombies par manche est de numero_manche*2.

## Effets
Tirer sur un ennemi le rend progresseivement blanc pendant 0.5 secondes.

## Environnement
La map est très sombre, mais un spotlight éclaire le centre du champ de vision du joueur.
La scène est très simple: il y a des murs en bois sur le côté pour empêcher le joueur et les zombies de s'éloigner, un mur en guise d'obstacle dans la map pour permettre au joueur de se cacher ainsi qu'on bâtiment (tout est en low poly).

## Modélisation, ossement, animation
J'ai modélisé, ossé et animé le personnage sur Blender, et j'ai importé les quelques éléments du décor pour gagner du temps.
Il possède 3 animations: idle, courir, sauter. (Donc appuyer sur maj augmente la vitesse du personnage mais ne change pas l'animation.)

## Sons
Voici les sons existants:  
-Son d'ambiance   
-Le zombie émet un cri en mourrant  
-Son du tir des zombies  
-Son du tir du personnage  
-Son du saut  
-Son des pas  
-Son du heal  
-Rire machiavélique à chaque fin de round.  
